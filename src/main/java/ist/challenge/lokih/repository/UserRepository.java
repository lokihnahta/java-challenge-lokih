package ist.challenge.lokih.repository;


import ist.challenge.lokih.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
@Transactional
public interface UserRepository extends JpaRepository<User, Long> {
    public Optional<User> findById(Long id);
    public User findByUsername(String username);

}

