package ist.challenge.lokih.implementation;

import com.fasterxml.jackson.databind.ObjectMapper;
import ist.challenge.lokih.entity.User;
import ist.challenge.lokih.repository.UserRepository;
import ist.challenge.lokih.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class UserServiceImplementation implements UserService {
    @Autowired
    UserRepository userRepository;

    private ObjectMapper om = new ObjectMapper();

    @Override
    public List<User> getAllUsers() {
        try {
            List<User> users = new ArrayList<User>();
            userRepository.findAll().forEach(users::add);
            return users;
        } catch (Exception e) {
            return null;
        }

    }
    @Override
    public HttpStatus createUser(String jsonBody) {
        try {
            Map<String, Object> jsonNode = om.readValue(jsonBody, Map.class);
            String username = (jsonNode.get("username") == null ? "" : (jsonNode.get("username").toString()));
            String password = (jsonNode.get("password") == null ? "" : (jsonNode.get("password").toString()));

            User checkUser = userRepository.findByUsername(username);

            if (checkUser==null) { //if no user found with the username given then proceed
                userRepository.saveAndFlush(new User(username, password));
                return HttpStatus.CREATED;
            }
            else{ //if found return 409
                return HttpStatus.CONFLICT;
            }

        } catch (Exception e) {
            return HttpStatus.BAD_REQUEST;
        }
    }
    @Override
    public HttpStatus login(String jsonBody) {
        try {
            Map<String, Object> jsonNode = om.readValue(jsonBody, Map.class);
            String username = (jsonNode.get("username") == null ? "" : (jsonNode.get("username").toString()));
            String password = (jsonNode.get("password") == null ? "" : (jsonNode.get("password").toString()));

            User checkUser = userRepository.findByUsername(username);

            if (username.isEmpty() || password.isEmpty()) { //if username / password empty return 400
                return HttpStatus.BAD_REQUEST;
            }
            else if (checkUser!=null) { //if found user with the given username proceed
                //if username and password match return 200
                if (username.equals(checkUser.getUserName()) && password.equals(checkUser.getPassword()))
                    return HttpStatus.OK;
                else
                    return HttpStatus.UNAUTHORIZED;
            }
            else{
                return HttpStatus.NOT_FOUND;
            }

        } catch (Exception e) {
            return HttpStatus.BAD_REQUEST;
        }

    }
    @Override
    public HttpStatus editUser(String jsonBody) {
        try {
            Map<String, Object> jsonNode = om.readValue(jsonBody, Map.class);
            Long id = jsonNode.get("id") == null ? -99L : Long.valueOf(jsonNode.get("id").toString());
            String username = (jsonNode.get("username") == null ? "" : (jsonNode.get("username").toString()));
            String password = (jsonNode.get("password") == null ? "" : (jsonNode.get("password").toString()));

            if(id>0){ //if id > 0 then proceed
                User user = userRepository.findById(id).get();
                User checkUser = userRepository.findByUsername(username);

                if (user!=null) { //if found user with given id then proceed
                    if (password.equals(user.getPassword())) //if password same with old password return 400
                        return HttpStatus.BAD_REQUEST;
                    else{
                        if (checkUser!=null && checkUser.getId()!=id){ //if found same username with different id return 409
                            return HttpStatus.CONFLICT;
                        }
                        else{ //if all pass proceed update / edit user and return 201
                            user.setUserName(username);
                            user.setPassword(password);
                            userRepository.saveAndFlush(user);
                            return HttpStatus.CREATED;
                        }
                    }
                }
                else{ //if can not found return 404
                    return HttpStatus.NOT_FOUND;
                }
            }
            else //if can not found return 404
                return HttpStatus.NOT_FOUND;

        } catch (Exception e) {
            return HttpStatus.BAD_GATEWAY;
        }
    }


}
