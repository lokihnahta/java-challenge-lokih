package ist.challenge.lokih.service;

import ist.challenge.lokih.entity.User;
import org.springframework.http.HttpStatus;

import java.util.List;

public interface UserService{
    List<User> getAllUsers();
    HttpStatus createUser(String jsonBody);
    HttpStatus login(String jsonBody);
    HttpStatus editUser(String jsonBody);
}