package ist.challenge.lokih;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan
public class LokihApplication {

	public static void main(String[] args) {
		SpringApplication.run(LokihApplication.class, args);
	}

}
