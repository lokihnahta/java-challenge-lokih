package ist.challenge.lokih.controller;

import ist.challenge.lokih.entity.User;
import ist.challenge.lokih.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api")
public class UserController {

    @Autowired
    UserService userService;
    @GetMapping("/listUser")
    public ResponseEntity<List<User>> getAllUsers() {
        try {
            List<User> users = userService.getAllUsers();
            return new ResponseEntity<>(users, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/registrasi")
    public ResponseEntity createUser(@RequestBody String jsonBody)
    {
        try {
            HttpStatus _result = userService.createUser(jsonBody);
            String statesDesc = new String();
            if (_result==HttpStatus.OK)
                statesDesc = "Sukses";
            else if (_result==HttpStatus.CONFLICT)
                statesDesc = "Username sudah terpakai";
            return  new ResponseEntity(statesDesc,null,_result);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/login")
    public ResponseEntity login(@RequestBody String jsonBody)
    {
        try {
            HttpStatus _result = userService.login(jsonBody);
            String statesDesc = new String();
            if (_result==HttpStatus.OK)
                statesDesc = "Sukses";
            else if (_result==HttpStatus.BAD_REQUEST)
                statesDesc = "Username dan / atau password kosong";
            else if (_result==HttpStatus.UNAUTHORIZED)
                statesDesc = "Password salah";
            else if (_result==HttpStatus.NOT_FOUND)
                statesDesc = "Username tidak ditemukan";
            return  new ResponseEntity(statesDesc,null,_result);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/editUser")
    public ResponseEntity editUser(@RequestBody String jsonBody)
    {
        try {
            HttpStatus _result = userService.editUser(jsonBody);
            String statesDesc = new String();
            if (_result==HttpStatus.OK)
                statesDesc = "Sukses";
            else if (_result==HttpStatus.CONFLICT)
                statesDesc = "Username sudah terpakai";
            else if (_result==HttpStatus.NOT_FOUND)
                statesDesc = "Id tidak ditemukan";
            else if (_result==HttpStatus.BAD_REQUEST)
                statesDesc = "Password tidak boleh sama dengan password sebelumnya";
            return  new ResponseEntity(statesDesc,null,_result);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
