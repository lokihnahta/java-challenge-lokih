package ist.challenge.lokih;

import com.fasterxml.jackson.databind.ObjectMapper;
import ist.challenge.lokih.controller.UserController;
import ist.challenge.lokih.entity.User;
import ist.challenge.lokih.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(UserController.class)
public class UserControllerTest {
    @Autowired
    MockMvc mockMvc;
    @Autowired
    ObjectMapper mapper;

    @MockBean
    UserRepository userRepository;

    User RECORD_1 = new User("loki", "1111");

    @Test
    public void registrasi_success() throws Exception {
        String urlTemplate = "/api/registrasi";
        Mockito.when(userRepository.save(RECORD_1)).thenReturn(RECORD_1);

        MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.post(urlTemplate)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(this.mapper.writeValueAsString(RECORD_1));

        mockMvc.perform(mockRequest)
                .andExpect(status().isCreated());
    }

    @Test
    public void listUser_success() throws Exception {
        String urlTemplate = "/api/listUser";
        List<User> records = new ArrayList<>(Arrays.asList(RECORD_1));

        Mockito.when(userRepository.findAll()).thenReturn(records);

        mockMvc.perform(MockMvcRequestBuilders
                .get(urlTemplate)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].userName", is(RECORD_1.getUserName())));
    }

    @Test
    public void login_failed() throws Exception {
        String urlTemplate = "/api/login";

        Mockito.when(userRepository.findByUsername(RECORD_1.getUserName())).thenReturn(RECORD_1);

        MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.post(urlTemplate)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(this.mapper.writeValueAsString(RECORD_1));

        mockMvc.perform(mockRequest)
                .andExpect(status().isBadRequest());
    }

    @Test
    public void login_success() throws Exception {
        String urlTemplate = "/api/login";
        Mockito.when(userRepository.findByUsername(RECORD_1.getUserName())).thenReturn(RECORD_1);

        MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.post(urlTemplate)
                .content(this.mapper.writeValueAsString(RECORD_1).toLowerCase());

        mockMvc.perform(mockRequest)
                .andExpect(status().isOk());
    }
}
